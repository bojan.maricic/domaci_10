<?php

  require("include/config.php");
  require("include/db.php");
  require("include/functions.php");

?>

<form action="realinsert.php" method="POST" enctype="multipart/form-data">
  <fieldset class="fieldset">
    <legend class="legend">Dodaj proizvod</legend>
    <label>Naziv proizvoda</label><br>
    <input type="text" name="name" value="" required="required"><br><br>
    <label>Opis</label><br>
    <textarea type="text" name="description"  value="" required="required"></textarea><br><br>
    <label>Kategorija</label><br>
      <select type="text" name="category"  value="" required="required">
        <option value="">- Izaberi -</option>";
        <?php

        $sql = "SELECT * FROM category ORDER BY name ASC";
        $result = mysqli_query($connection,$sql) or die(mysql_error());

        if (mysqli_num_rows($result)>0)
        {
          while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
            echo "<option value=\"$record[id_category]\">$record[name]</option>";
        }

        ?>
      </select><br><br>
    <label>Slika</label><br>
    <input type="file" name="file" value="" required="required"><br><br>
    <label>Cena</label><br>
    <input type="text" name="price" value="" required="required"><br><br>
    <input type="submit" name="submitbutton" id="submitbutton" value="insert">
    <?php
    echo "<a href=\"index.php\">Go back</a><br>";
    ?>
  </fieldset>
</form>