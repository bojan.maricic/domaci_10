<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Car Dealer</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<meta property="og:title" content="Car Dealer"/>
	<meta property="og:description" content="Novi i polovni automobili" />
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" media="screen"/>
	<link rel="stylesheet" href="css/flaticon/flaticon.css" media="screen"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/javascript.js"></script>

	</head>

	
	<body>
		<div class="wrapper">
		
			<?php require("include/header.php"); ?>

			<!--CONTAINER-->
			<div class="container">
						
			<?php require("include/left.php"); ?>
		
			<?php require("include/products.php"); ?>
				
			</div>
			
			<?php require("include/footer.php"); ?>
		</div>
	</body>
</html>
