			<footer>
				<div class="footer__container">
				<div class="footer__about">
				<h3>O kompaniji</h3>
				<img src="images/logo-small.png">
				<p>lorem ipsum dolores sit amanet it. lorem ipsum dolores sit amanet it. lorem ipsum dolores sit amanet it.lorem ipsum dolores sit amanet it.</p>
				</div>
				<div class="footer__link">
					<h3>Navigacija</h3>
						<ul>
							<li><i class="icon icon-arrow-right"></i><a href="#">Početna</a></li>
							<li><i class="icon icon-arrow-right"></i><a href="#">O nama</a></li> 
							<li><i class="icon icon-arrow-right"></i><a href="#">Kategorije</a></li> 
							<li><i class="icon icon-arrow-right"></i><a href="#">Akcije</a></li>
							<li><i class="icon icon-arrow-right"></i><a href="#">Kontakt</a></li> 
						</ul>
				</div>
				<div class="footer__contact">
					<h3>Kontakt</h3>
						<ul><!-- latest posts widget starts-->
							<li><i class="icon icon-home"></i>Vojvođanska 50, Sombor</li>
							<li><i class="icon icon-phone"></i>+381 25 444 000</li>
							<li><i class="icon icon-mobile-phone"></i>+381 63 444 000</li>
							<li><i class="icon icon-envelope"></i> bojan@automotive.com</li>
						</ul>
				</div>
			
				<div class="footer__bottom">
					<p>&copy; Copyright 2018 <a href="index.php"><strong>Automotive</strong></a> | Izrada: <a href="#"><strong>Bojan</strong></a></p>	
				</div>
				</div>		
			</footer>