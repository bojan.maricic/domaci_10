<?php
 session_start();

   if(!isset($_SESSION['cart']))
   {
   	   	$_SESSION['cart'] = array();   	
   }

  require("include/config.php");
  require("include/db.php");
  require("include/functions.php");
  
  
 ?>
 
 
 
<header class="header">
					<div class="header__top">
						<div class="header__box--social">
							<a class="social" href="#" target="_blank"><i class="icon icon-facebook"></i></a> 
							<a class="social" href="#" target="_blank"><i class="icon icon-twitter"></i></a> 
							<a class="social" href="#" target="_blank"><i class="icon icon-linkedin"></i></a> 
						</div>
							
						<div class="header__box--contact header-contacts" >
							<i class="icon icon-envelope"></i> bojan@automotive.com 
						</div>
						<div class="header__box--contact header-contacts" >
							<i class="icon icon-phone"></i> 025 440 006
						</div>
						<div class="header__box--contact header-contacts" >
							<i class="icon icon-home"></i> Vojvođanska 50, Sombor
						</div>
					</div>
					
				<div class="header__menu">
			
					<div id="logo">
						<a href="index.php"><img src="images/logo.png" alt=""/></a>
					</div>

						

					<!-- MENU -->
					<nav class="menu">
						<ul class="navigation">
							<li class="active"><a href="index.php">Početna</a></li>
							<li><a href="#">O nama</a></li>
							<li class="has-child">
								<a href="#">Kategorije</a>
								<ul>
									<li><a href="#">Link 1</a></li>
									<li><a href="#">Link 2</a></li>
									<li><a href="#">Link 3</a></li>
									<li><a href="#">Link 4</a></li>
									<li><a href="#">Link 5</a></li>
								</ul>
							</li>
							<li><a href="#">Akcije i popusti</a></li>
							<li><a href="#">Kontakt</a></li>
						</ul>
					</nav>
					<!--KRAJ MENU -->
				</div>
				
				<!-- SLIDER -->
				<div class="slider">
				
					<div class="opening-time">
						<div class="time-table">
						<h3>RADNO VREME</h3>
							<div class="inner-bg">
								<dl class="week-day">
								<dt>Ponedeljak</dt>
								<dd>09:00-18:00</dd>
								</dl>
								<dl class="week-day light-bg">
								<dt>Utorak</dt>
								<dd>09:00-18:00</dd>
								</dl>
								<dl class="week-day">
								<dt>Sreda</dt>
								<dd>09:00-18:00</dd>
								</dl>
								<dl class="week-day light-bg">
								<dt>Četvrtak</dt>
								<dd>09:00-18:00</dd>
								</dl>
								<dl class="week-day">
								<dt>Petak</dt>
								<dd>09:00-18:00</dd>
								</dl>
								<dl class="week-day light-bg">
								<dt>Subota</dt>
								<dd>09:00-13:00</dd>
								</dl>
								<dl class="week-day closed">
								<dt>Nedelja</dt>
								<dd>Neradan dan</dd>
								</dl>
							</div>
						</div>
					</div>
					
				</div>
				<!--KRAJ SLIDER-->
				
			</header>
			<!--KRAJ HEADER-->
<?php


	if(isset($_SESSION['id_user']))
	{
		$name = get_user_name($_SESSION['id_user']);
		echo "You are logged as <b>$name</b> ";
		echo "| <a href=\"cart_content.php\" >Korpa</a> ";
		echo "| <a href=\"logout.php\" >Odjavi se!</a>&nbsp; ";		
	}
	else 
	{
		echo "<form method=\"post\" name=\"login\" action=\"login.php\">
			  Korisnik: <input type=\"text\" name=\"user\" size=\"10\" />
			  Šifra: <input type=\"password\" name=\"password\" size=\"10\" />
			  <input type=\"submit\" name=\"sb\" value=\"Prijavi se!\" />
			  </form>\n";
	}
			  			  		


?>
