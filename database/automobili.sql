-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 17, 2018 at 10:35 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automobili`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_order`
--

DROP TABLE IF EXISTS `cart_order`;
CREATE TABLE IF NOT EXISTS `cart_order` (
  `id_cart_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `order_text` text COLLATE utf8_unicode_ci NOT NULL,
  `total_price` float NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_cart_order`),
  KEY `id_users` (`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_order_item`
--

DROP TABLE IF EXISTS `cart_order_item`;
CREATE TABLE IF NOT EXISTS `cart_order_item` (
  `id_cart_order_item` int(11) NOT NULL AUTO_INCREMENT,
  `id_cart_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `amount` smallint(6) NOT NULL,
  PRIMARY KEY (`id_cart_order_item`),
  KEY `id_order` (`id_cart_order`,`id_product`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart_order_item`
--

INSERT INTO `cart_order_item` (`id_cart_order_item`, `id_cart_order`, `id_product`, `amount`) VALUES
(1, 1, 1, 34),
(2, 1, 3, 14),
(3, 2, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `name`) VALUES
(1, 'Delovi za motor'),
(2, 'Dodaci za auto');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `name` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id_product`),
  KEY `id_category` (`id_category`),
  KEY `id_category_2` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `id_category`, `name`, `description`, `image`, `price`) VALUES
(8, 1, 'Menjač za Suzuki', 'Nov menjač za suzuki, potpuno ispravan. Prodajem hitno!', 'car4.jpg', 200),
(9, 1, 'Hladnjak', 'Prodajem polovan hladnjak za Toyotu.', 'car2.jpg', 500),
(10, 2, 'Retrovizor', 'Polovan, očuvan retrovizor za Toyotu', 'car2.jpg', 20),
(11, 2, 'Brisači', 'Brisači kao novi.', 'car3.jpg', 10),
(12, 1, 'alternator', 'Nov alternator na prodaju', 'car6.jpg', 200),
(13, 1, 'Kvačilo', 'Prodajem kvačilo, hitno!!!', 'car5.jpg', 100);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` int(1) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `roles_id` (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `email`, `birthday`, `firstname`, `lastname`, `active`, `roles_id`) VALUES
(1, 'root', '6a0762d51c3a98ac26fbe5a4a123106e', 'root@predavanja.com', '2017-12-03', 'Bojan', 'Maričić', 1, 1),
(2, 'predavanja', '6a0762d51c3a98ac26fbe5a4a123106e', 'predavanja@predavanja.com', '2017-12-03', 'petar', 'petrovic', 1, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
