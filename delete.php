<?php

   session_start();
   
   if(!isset($_SESSION['id_user']))
   {
   		header("Location:index.php");
   		exit();
   }
   
   $id_product = (int)$_GET['id_product'];
  
   if(is_int($id_product))
   {
   		unset($_SESSION['cart'][$id_product]);
   }

  
   header("Location:cart_content.php");
   exit();
   
  
 ?>