<?php

   session_start();
   
   if(!isset($_SESSION['id_user']))
   {
   		header("Location:index.php");
   		exit();
   }

   require("include/config.php");
   require("include/db.php");

   $amount = (int)mysqli_real_escape_string($connection,$_POST['amount']);
   $id_product = (int)mysqli_real_escape_string($connection,$_POST['id_product']);

   if(is_numeric($amount) AND $amount>0)
   {
   	//$_SESSION['cart'][$id_product] = $amount+$_SESSION['cart'][$id_product];
    $_SESSION['cart'][$id_product] += $amount; // $_SESSION['cart'][3] = 12;
   }

   header("Location:cart_content.php");
   exit();

 ?>